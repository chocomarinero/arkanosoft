import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private conf = {
    key: 'b23df7a00547ea84dee2d03dbe012bb3',
    latlong: '-33.4726900,-70.6472400',
    exclude: 'minutely,hourly,daily,alerts,flags',
    lang: 'es',
    units: 'si'
  };
  private darkskyUrl: any;
  constructor(private http: HttpClient) {
    this.darkskyUrl = `https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/${this.conf.key}/${this.conf.latlong}?exclude=${this.conf.exclude}&lang=${this.conf.lang}&units=${this.conf.units}`;
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  getCurrentWeather(): Observable<any> {
    return this.http.get<any>(this.darkskyUrl)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
