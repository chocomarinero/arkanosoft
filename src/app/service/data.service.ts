import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dashboardData: any;
  constructor() {
    this.dashboardData = [];
  }

  getData(){
    return this.dashboardData;
  }
  setData(newData){
    const newElement = {
      date: newData.fechaIngreso,
      data: [
        { num: newData.nuevasCompras, name: 'Nuevas Compras', icon: 'shopping-bag', color: 'blue' },
        { num: newData.porcentajeIncrementoCompras, percentage: true, name: 'Incremento de Compras', icon: 'bar-chart', color: 'green'},
        { num: newData.nuevosUsuarios, name: 'Nuevos Usuarios', icon: 'user-plus', color: 'yellow' },
        { num: newData.nuevasVisitas, name: 'Nuevas Visitas', icon: 'pie-chart', color: 'red' },
      ]
    };
    this.dashboardData.push(newElement);
  }
}
