import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.scss']
})
export class DashboardCardComponent implements OnInit {
  @Input() name: string;
  @Input() num: string;
  @Input() percentage: boolean;
  @Input() icon: string;
  @Input() color: string;
  darkenColor: string;
  constructor() {}

  ngOnInit() {
    this.darkenColor = 'darken(' + this.color + ',50%) !important';
  }

}
