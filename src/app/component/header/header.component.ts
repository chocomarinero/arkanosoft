import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../service/weather.service';
import { Observable } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  weatherObservable$: Observable<any>;
  constructor(public weatherService: WeatherService ) {
    this.weatherObservable$ = this.weatherService.getCurrentWeather();
  }
  ngOnInit() {}
  toggleSidebar() {
    $('#wrapper').toggleClass('toggled');
  }

}
