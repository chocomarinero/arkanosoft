import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Componentes
import { DashboardCardComponent } from './component/dashboard-card/dashboard-card.component';
import { HeaderComponent } from './component/header/header.component';

// Paginas
import { DataInputComponent } from './page/data-input/data-input.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';

// Servicios
import { DataService } from './service/data.service';
import { WeatherService } from './service/weather.service';

// Librerias
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { ToastrModule } from 'ngx-toastr';

// Definicion de local
defineLocale('es', esLocale);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DashboardCardComponent,
    DataInputComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [DataService, WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
