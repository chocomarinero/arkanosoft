import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales } from 'ngx-bootstrap/chronos';
import { DataService } from '../../service/data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-data-input',
  templateUrl: './data-input.component.html',
  styleUrls: ['./data-input.component.scss']
})
export class DataInputComponent implements OnInit {
  public form: FormGroup;
  public formDisplay: boolean;
  private formSubmitAttempt: boolean;
  minDate: Date;
  locale = 'es';
  locales = listLocales();
  constructor(
    private localeService: BsLocaleService,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private toastr: ToastrService) {
      this.minDate = new Date();
      this.minDate.setDate(this.minDate.getDate() + 1);
      this.localeService.use('es');
      this.formDisplay = true;
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      nuevasCompras                 : [null,  [Validators.required]],
      porcentajeIncrementoCompras   : [null,  [Validators.required]],
      nuevosUsuarios                : [null,  [Validators.required]],
      nuevasVisitas                 : [null,  [Validators.required]],
      fechaIngreso                  : [null,  [Validators.required]],
    });
  }
  esValido(field: string) {
    if (field === 'tipo') {
      return (this.form.get(field).touched && this.form.get(field).value === 0);
    } else {
      return (!this.form.get(field).valid && this.form.get(field).touched) || (this.form.get(field).untouched && this.formSubmitAttempt);
    }
  }

  muestraCampoCss(field: string) {
    return {
      'is-invalid': this.esValido(field),
      'has-feedback': this.esValido(field)
    };
  }

  validaFormulario(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validaFormulario(control);
      }
    });
  }

  onSubmit() {
    this.formSubmitAttempt = true;
    if (this.form.valid) {
      const data = { ...this.form.value };
      data.fechaIngreso = this.formatDate(data.fechaIngreso);
      this.dataService.setData(data);
      this.form.reset();
      this.formSubmitAttempt = false;
      this.formDisplay = false;
      this.toastr.success('El elemento fue agregado al dashboard', 'Éxito', {
        positionClass: 'toast-bottom-right'
      });
    } else {
      this.validaFormulario(this.form);
    }
  }

  formatDate(date: Date){
    const formatted = date.toLocaleString();
    return formatted.slice(0, formatted.indexOf(' '));
  }
}
