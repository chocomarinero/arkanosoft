# ArkanoSoft

Proyecto de postulación para Arkanosoft

## Librerias utilizadas

* [Angular](https://angular.io/docs) : Se utiliza la versión 8.2 del framework
* [ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#/datepicker) : Libreria de componentes, se utiliza componente de datepicker
* [ngx-toastr](https://scttcper.github.io/ngx-toastr/) : Libreria para toasts
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)

## API Externa

Para obtener la información del clima, se utiliza la siguiente API:
* [Dark Sky API](https://darksky.net/dev) : 

## Desarrollo

1. Instalar paquetes con `npm install`
2. Correr `ng serve` para levantar un ambiente de desarrollo.
3. Navegar a `http://localhost:4200/`.

## Build
1. Correr `ng build` para hacer build al proyecto.
2. Lo generado por el comando sera almacenado en el directorio `dist/` .
